import pandas as pd
import seaborn as sns

import xpresso.ai.core.data.visualization.utils as utils
from xpresso.ai.core.data.visualization.seaborn.res.plot_factory import Plot


class NewPlot(Plot):
    """
    Generates a box plot
    Attributes:
        plot(:obj: matplotlib.axes): Matplotlib axes which stores the plot
    Args:
        input_1(list): data for box plot
        target(list): target variable data
        plot_title(str): Title to the plot
        output_format (str): html or png plots to be generated
        file_name(str): File name of the plot to be stored
        axes_labels(dict): Dictionary of x_label, y_label and
            target_label
        output_path(str): path where the html/png plots to be stored
    """

    def __init__(self, input_1, target=None, plot_title=None,
                 output_format=utils.HTML,
                 file_name=None, axes_labels=None,
                 output_path=utils.DEFAULT_IMAGE_PATH):

        super().__init__()
        sns.set(font_scale=1.5)
        if axes_labels:
            self.axes_labels = axes_labels
        if target is None:
            self.base_plot(input_1)
        else:
            self.target_plot(input_1, target)
        self.plot.set(xlabel=self.axes_labels[utils.X_LABEL],
                      ylabel=self.axes_labels[utils.Y_LABEL],
                      title=plot_title)
        self.render(output_format=output_format, output_path=output_path,
                    file_name=file_name)

    def base_plot(self, input_1):
        """
        Plots the box plot without target variable
        Args:
            input_1(:list): Data for plot
        """
        self.plot = sns.boxplot(x=input_1, orient='v')

    def target_plot(self, input_1, target):
        """
        Plots the box plot with target variable
        Args:
            input_1(:list): Data for base bar plot
            target(:list): Target variable data
        """
        data = pd.DataFrame(list(zip(input_1, target)))
        try:
            categorical = 1
            self.plot = sns.boxplot(x=1, y=0, data=data, orient="v")
        except TypeError:
            categorical = 0
            self.plot = sns.boxplot(x=0, y=1, data=data, orient="v")
        self.set_xticks_labels(data[categorical])

    def set_xticks_labels(self, data):
        """
        Customize the xtick labels
        Args:
            data(:list): Data for the plot
        """
        xticks_labels = self.plot.xaxis.get_ticklabels()
        data = data.value_counts().to_dict()
        temp_xticks = list()
        total_data = sum(data.values())
        for xtick in xticks_labels:
            xtick_text = xtick.get_text()
            try:
                category_count_percent = data[xtick_text] / total_data * 100
                temp_xticks.append(
                    "{} ({}%)".format(xtick_text,
                                      round(category_count_percent, 2)))
                self.plot.set_xticklabels(temp_xticks)
            except KeyError:
                pass
